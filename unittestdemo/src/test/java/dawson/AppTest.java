package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void testEcho() {
        App app = new App();

        int input = 5;

        assertEquals("this checks if in fact echo returns the same value it is inputed", input, app.echo(input));
    }
    @Test
    public void testOneMore() {
        App app = new App();

        int input = 5;

        assertEquals("this checks if in fact oneMore returns on more than the value it is inputed", input+1, app.echo(input));
    }
}
